const cluster = require("cluster")
const http = require("http")
const numCPUs = require("os").cpus().length

// 当该进程是主进程时，返回 True
if (cluster.isMaster) {
  console.log(`主进程 pid:${process.pid} 正在运行`)

  // 衍生工作进程，根据 CPU 个数
  for (let i = 0; i < numCPUs; i++) {
    cluster.fork()
  }

  cluster.on("exit", (worker, code, signal) => {
    console.log(`工作进程 pid:${worker.process.pid} 已退出`)
  })
} else {
  // 工作进程可以共享任何 TCP 连接。
  // 在本例子中，共享的是一个 HTTP 服务器。
  http
    .createServer((req, res) => {
      res.writeHead(200)
      res.end("你好世界\n")
    })
    .listen(8000)

  console.log(`server run in 127.0.0.1:8000`)
  console.log(`工作进程 pid:${process.pid} 已启动`)
}
