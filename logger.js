const colors = require("colors")

const logger = async (ctx, next) => {
  const start = Date.now()
  await next()
  const duration = Date.now() - start
  console.log(`请求方法${ctx.method}`.green, `持续时间${duration}`.red)
}

module.exports = logger
