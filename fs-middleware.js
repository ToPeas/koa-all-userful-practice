const fs = require("fs")

module.exports = page => {
  return new Promise((resolver, reject) => {
    const view = `./view/${page}`
    fs.readFile(view, "binary", (err, data) => {
      err ? reject(err) : resolver(data)
    })
  })
}
