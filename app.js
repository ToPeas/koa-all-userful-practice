const koa = require("koa")
const app = new koa()
const bodyParser = require("koa-bodyparser")
const cors = require("kcors")
const logger = require("koa-logger")
// const logger = require("./logger")
// const ban = require("./ban-favicon")
// const session = require("./session")
const koaRouter = require("koa-router")
const fs = require("fs")
const busboy = require("./file-upload/busboy")
const router = new koaRouter()

app.use(logger())
// app.use(ban)
// app.use(async (ctx, next) => {
//   ctx.body = {
//     data: "数据"
//   }
// })

app.use(cors())

// app.use(session)
app.use(bodyParser())

// 文件上传
router.post("/formdata", async ctx => {
  ctx.body = await busboy(ctx)
})

app.use(router.routes())
app.use(router.allowedMethods())

app.use(async ctx => {
  ctx.body = "我是默认的接口"
})
app.listen(4444, err => {
  if (err) {
    console.log(err)
  }
  console.log("开始监听端口4444")
})
