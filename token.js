const Koa = require('koa');
const Router = require('koa-router');
const render = require('koa-art-template');
const path = require('path');
const app = new Koa();
const router = new Router();
render(app, {
    root: path.join(__dirname, 'views'),     // views 路径
    extname: '.html',
    debug: process.env.NODE_ENV !== 'production'
});
// 自定义用户认证中间件
const auth = () => async (ctx, next) => {
    const sid = ctx.cookies.get('sid');
    if (sid !== undefined) {
        const res = await ctx.session.find(sid);
        if (res === null || res === false) {
            ctx.cookies.set('sid', undefined);
            return ctx.render('index');
        }
    }
    else {
        // 访问 login 时，不用重定向
        if (ctx.url === '/login') return await next();
        return ctx.render('index');
    }
    await next();
}
router
    .get('/', async ctx => {
        await ctx.body = { sid: ctx.cookies.get('sid') }
    })
    .get('/home', async ctx => {
      await ctx.body = { sid: ctx.cookies.get('sid') }
        // await ctx.render('index-home', { sid: ctx.cookies.get('sid') });
    })
    .post('/login', async ctx => {
        if (!ctx.cookies.get('sid')) {
            // 第一次登录
            const data = ctx.request.body;
            // 创建 session,
            const sid = await ctx.session.create(data.user);
            // 设置 cookies
            ctx.cookies.set('sid', sid);
            console.log(sid);
            return ctx.redirect('/home');
        }
        ctx.redirect('/');
    })
    .get('/logoff', async ctx => {
        await ctx.session.delete(ctx.cookies.get('sid'));
        ctx.cookies.set('sid', undefined);
        ctx.redirect('/');
    })
    ;
app
    .use(require('koa-logger')())
    // .use(require('koa-static-cache')(path.join(__dirname, './public')))
    .use(require('koa-body')())
    .use(require('./session')())
    .use(auth())
    .use(router.routes())
    .use(router.allowedMethods())
app.listen(3000, () => {
    console.log('\n\trun server in 3000.');
});