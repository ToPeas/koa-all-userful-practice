const busboy = require("busboy")
const mkdirs = require("mkdirs")
const path = require("path")
const fs = require("fs")
const Busboy = (ctx, options = {}) => {
  return new Promise((resolve, reject) => {
    let busboyobj = new busboy({ headers: ctx.headers })
    busboyobj.on("file", (fieldname, file, filename, encoding, mimetype) => {
      name = Date.now() + "_" + path.basename(filename)
      file.pipe(fs.createWriteStream(path.join(__dirname, "images/") + name))
    })
    busboyobj.on("finish", function() {
      console.log("文件上传成功")
      resolve({
        success: true,
        file: name
      })
    })
    busboyobj.on("error", function(err) {
      console.log("文件上出错")
      reject({
        success: false,
        file: null
      })
    })
    ctx.req.pipe(busboyobj)
  })
}

module.exports = Busboy
