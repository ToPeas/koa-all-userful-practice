const http = require("http")
const fetch = require("node-fetch")
const axios = require("axios")
// const proxy = http.createServer((request, response) => {
//   response.setHeader(
//     "Content-Type",
//     "application/json;charset=utf-8;text/plain"
//   )
//   fetch("http://news-at.zhihu.com" + request.url, {
//     method: request.method,
//     headers: { "Content-Type": "application/json" }
//   })
//     .then(res => res.body.pipe(response))
//     .catch(err => {
//       console.error("Error:", err)
//     })
// })
// proxy.listen(3006, () => {
//   console.log("\n\tserver run in 3000\n")
// })

const proxy = http.createServer((request, response) => {
  response.setHeader(
    "Content-Type",
    "application/json;charset=utf-8;text/plain"
  )
  axios({
    method: "get",
    url: "http://news-at.zhihu.com" + request.url,
    headers: {
      "Content-Type": "application/json;charset=utf-8"
    },
    responseType: "stream"
  })
    .then(res => res.data.pipe(response))
    .catch(err => {
      console.log(err)
    })
})

proxy.listen(3006, () => {
  console.log("\n\tserver run in 3000\n")
})
