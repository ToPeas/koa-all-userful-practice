const http = require("http")
const httpProxy = require("http-proxy")
// 新建一个代理 Proxy Server 对象
const proxy = httpProxy.createProxyServer({})
// 捕获错误
proxy.on("error", (err, req, res) => {
  console.error("Error:", err)
  res.writeHead(500, { "Content-Type": "text/plain" })
  res.end("发生了错误")
})
// 另外新建一个 HTTP 80 端口的服务器，也就是常规 Node 创建 HTTP 服务器的方法。
// 在每次请求中，调用 proxy.web(req, res config) 方法进行请求分发
const server = http.createServer((req, res) => {
  // 在这里可以自定义你的路由分发
  let host = req.headers.host,
    ip = req.headers["x-forwarded-for"] || req.connection.remoteAddress
  console.log("client ip:", ip, "host:", host)
  switch (host) {
    case "a.com":
    case "bbs.a.com":
      return proxy.web(req, res, { target: "http://127.0.0.1:3000" })
    case "b.com":
      return proxy.web(req, res, { target: "http://127.0.0.1:3001" })
    case "c.com":
    case "www.c.com":
      return proxy.web(req, res, { target: "http://127.0.0.1:3002" })
    case "d.cn":
      return proxy.web(req, res, { target: "http://127.0.0.1:3003" })
    default:
      res.writeHead(200, { "Content-Type": "text/plain" })
      res.end("Welcome to my server!")
  }
})
server.listen(3006, () => {
  console.log("\n\tserver run in 3000\n")
})
