const zlib = require("zlib")
const fs = require("fs")
const util = require("util")
const path = require("path")

// promisify
const lstat = util.promisify(fs.lstat)
const readdir = util.promisify(fs.readdir)

/**
 * 根据路径生成 Gzip 压缩文件
 */
function Gzip(path) {
  const gzip = zlib.createGzip()
  const inp = fs.createReadStream(path)
  const out = fs.createWriteStream(path + ".gz")
  inp.pipe(gzip).pipe(out)
}

/**
 *  在静态目录里寻找 js 和 css 文件
 * @return{Array} 文件数组
 */
async function findJsOrCss(staticDir, types = ["js", "css"]) {
  let arr = []
  // 是文件夹
  // console.log(arr)
  if ((await lstat(staticDir)).isDirectory()) {
    const files = await readdir(staticDir)
    // 变量文件夹
    for (let file of files) {
      let _path = path.join(staticDir, file)
      let re = new RegExp(`\\.(${types.join("|")})$`)
      // 是文件夹则递归
      if ((await lstat(_path)).isDirectory()) {
        arr = arr.concat(await findJsOrCss(_path, types))
      } else if (re.test(_path)) {
        arr.push(_path)
      }
    }
  }

  return arr
}

// 开始
console.time("run")
// findJsOrCss(path.join(__dirname, './build/static'))
findJsOrCss(path.join(__dirname, "./src"))
  .then(files => files.forEach(f => Gzip(f)))
  .then(res => {
    console.timeEnd("run")
    console.log("全部压缩完成！")
  })
