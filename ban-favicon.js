const ban = async (ctx, next) => {
  if (ctx.url === "/favicon.ico") {
    console.log("此访问不通过")
    return  
  }
  await next()
}

module.exports = ban
