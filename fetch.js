// const { createReadStream } = require("fs")
// const fetch = require("node-fetch")
// const fs = require("fs")
// fetch(
//   "https://assets-cdn.github.com/images/modules/logos_page/Octocat.png"
// ).then(res => {
//   // 创建一个可写流
//   const dest = fs.createWriteStream("./octocat.png")

//   dest.on("end", () => {
//     console.log("end")
//   })

//   // 接上 res.body
//   res.body.pipe(dest)
// })

const axios = require("axios")

axios({
  method: "get",
  url: "http://bit.ly/2mTM3nY",
  responseType: "stream" // <-- 需要设置
})
  .then(function(response) {
    response.data.pipe(fs.createWriteStream("ada_lovelace.jpg"))
  })
  .catch(err => {
    console.log(err)
  })
